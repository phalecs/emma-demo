import {Column, Entity, OneToMany, PrimaryGeneratedColumn, Timestamp, PrimaryColumn} from "typeorm";
import { Account } from "./account";

@Entity()
export class Provider {

    @Column()
    public displayName: string;

    @Column()
    public logoUri: string;

    @PrimaryColumn()
    public providerId: string;

    @OneToMany((type) => Account, (account) => account.provider)
    public accounts?: Account[];

}
