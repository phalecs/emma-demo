import {Column, Entity, ManyToOne, PrimaryColumn, OneToMany} from "typeorm";
import { Customer } from "./customer";
import { Provider } from "./provider";
import { Transaction } from "./transaction";

@Entity()
export class Account {

    @PrimaryColumn()
    public accountId: string;

    @Column()
    public accountType: string;

    @Column()
    public currency: string;

    @Column()
    public displayName: string;

    @Column({nullable: true })
    public iban?: string;

    @Column()
    public accountNumber: string;

    @Column()
    public sortCode: string;

    @Column()
    public swiftBic: string;

    @Column()
    public updateTimestamp: Date;


    @ManyToOne(() => Customer, (customer) => customer.accounts)
    public customer?: Customer;

    @ManyToOne(() => Provider, (provider) => provider.accounts)
    public provider?: Provider;

    @OneToMany(() => Transaction, (tranaction) => tranaction.account)
    public transactions?: Transaction[];

    public static mapAccount(item: any) {
        const result: Account = {
            accountId: item.account_id,
            accountNumber: item.account_number.number,
            accountType: item.account_type,
            currency: item.currency,
            displayName: item.display_name,
            iban: item.account_number.iban,
            sortCode: item.account_number.sort_code,
            swiftBic: item.account_number.swift_bic,
            provider: {
                displayName: item.provider.display_name,
                logoUri: item.provider.logo_uri,
                providerId: item.provider.provider_id,
            },
            updateTimestamp: item.update_timestamp,
        };

        return result;
    }

}
