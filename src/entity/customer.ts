import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import { Account } from "./account";

@Entity()
export class Customer {

    @PrimaryGeneratedColumn()
    public id?: number;

    @Column()
    public fullName: string;

    @Column({nullable: true })
    public dateOfBirth?: Date;

    @Column({type: "text", array: true, nullable: true })
    public emails: string[];

    @Column({type: "text", array: true, nullable: true })
    public phones: string[];

    @Column()
    public updateTimestamp: Date;

    @OneToMany((type) => Account, (account) => account.customer)
    public accounts?: Account[];

    public static mapCustomer(apiCustomberObject: any) {
        const customer = new Customer();
        customer.fullName = apiCustomberObject.full_name;
        customer.dateOfBirth = apiCustomberObject.date_of_birth;
        customer.phones = apiCustomberObject.phones;
        customer.emails = apiCustomberObject.emails;
        customer.updateTimestamp = apiCustomberObject.update_timestamp;
        return customer;
    }

}
