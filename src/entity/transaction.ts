import { Entity, PrimaryColumn, Column, ManyToOne } from "typeorm";
import { Account } from "./account";


@Entity()
export class Transaction {

    @PrimaryColumn()
    transactionId: string;

    @Column()
    timestamp: Date;

    @Column()
    description: string;

    @Column()
    transactionType: string;

    @Column()
    transactionCategory: string;

    @Column({type: "text", array: true, nullable: true })
    transactionClassification: string[];

    @Column({nullable: true})
    merchantName?: string;

    @Column({type: "decimal"})
    amount: number;

    @Column()
    currency: string;

    @Column({type: "simple-json"})
    meta: any;

    @ManyToOne((type) => Account, (account) => account.transactions)
    public account?: Account;

    public static mapTransaction(item: any) {
        const result = new Transaction();
    
        result.amount = item.amount;
        result.currency = item.currency;
        result.description = item.description;
        result.merchantName = item.merchant_name;
        result.meta = item.meta;
        result.transactionCategory = item.transaction_category;
        result.transactionClassification = item.transaction_classificatio;
        result.transactionId = item.transaction_id;
        result.transactionType = item.transaction_type;
        result.timestamp = item.timestamp;

        return result;
        
    }

    public includeAccount(currentAccount: Account): Transaction {
        this.account = currentAccount;
        return this;
    }
}