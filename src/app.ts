import Axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";
import * as crypto from "crypto";
import * as dotenv from "dotenv";
import express from "express";
import * as _ from "lodash";
import { Account, Customer, Provider, Transaction } from "./entity/models";
import { LoginResponse, TokenRequest } from "./models/models";
import { ApiService, DatabaseService } from "./services/services";
import { Connection, Any, getConnectionOptions } from "typeorm";

export const app = express();

process.env.NODE_ENV === "test" ? dotenv.config({path: "tests.env"}) :dotenv.config();

// const environment = process.env as Environment;

const port = process.env.PORT;
const connectionOptions = getConnectionOptions();
const databaseService = new DatabaseService(connectionOptions);
const apiService = new ApiService(process.env.API_BASE_URI as string, databaseService);

app.get("/", (req, res) => res.send("Hello World!"));

app.get("/login", (req, res) => {
    const state = "a"; // crypto.randomBytes(20).toString("hex");
    res.redirect(`${process.env.AUTH_URI}&state=${state}`);
});

app.get("/callback", (request, response) => {
    const loginResponse: LoginResponse = apiService.extract_login_response(request.query);

    if (loginResponse.error === "access_denied" || !(loginResponse.code && loginResponse.state && loginResponse.scope)) {
        response.status(400);
        response.send("Error Retrieving Logged in User Information. Go to /login");
    } else {

        const tokenRequest = new TokenRequest(loginResponse.code).toFormData();
        const requestData = {
            config: { headers: tokenRequest.getHeaders()},
            data: tokenRequest,
            url: process.env.TOKEN_CONNECT_URI as string,
        };
        
        apiService.request_tokens(requestData.url, requestData.data, requestData.config).then((axiosResponse: AxiosResponse) => {
            apiService.updateTokens(axiosResponse.data);
            return apiService.getCustomerTransactions();
        }).then(([customer, transactions]: [Customer, Transaction[]]) => {
            response.send(`Transaction Retrieved, go to  View User Transactions /customer/${customer.id}/transactions`);
        })
        .catch((error: AxiosError) => {
            console.log(error.message);
            response.statusCode = 400;
            response.send("Error Retrieving Customer Transactions");
        });

    }

});

app.get('/customer/:customerId/transactions', async (req, res) => {
    const groupedTransactions = await apiService.getGroupedTransactions(req.params.customerId);
    const result = {
        "accounts": groupedTransactions
    }
    res.send(result);
});

app.get('/customer/:customerId/refresh', async (request, response) => {
    return apiService.getCustomerTransactions().then(([customer, transactions]) => {
        response.send(`Transaction Refreshed for User, go to  /customer/${customer.id}/transactions`);
    });
});

if (process.env.NODE_ENV !== 'test') {
    app.listen(port, () => console.log(`Application listening on port ${port}!`));
}


