import request from "supertest";
import nock from "nock";
import { app } from "../app";
import { ApiService } from "../services/services";


describe('GET /login', function() {
    it('Redirects to Login Page', function(done) {
      request(app)
        .get('/login')
        .expect(302)
        .end(function(err, res) {
          if (err) return done(err);
          done();
        });
    });
  });


 
  describe('GET /callback', function() {
    it('Retrieve Customer Transaction without parameters', function(done) {
      request(app)
        .get(`/callback`)
        .expect(400)
        .end(function(err, res) {
          if (err) return done(err);
          done();
        });
    });
  });

  describe('GET /callback', function() {
    it('Access Denied failure response', function(done) {
      request(app)
        .get(`/callback`)
        .query({
          error: "access_denied" })
        .expect(400)
        .end(function(err, res) {
          if (err) return done(err);
          done();
        });
    });
  });


  describe('GET /callback', function() {

    it('Token Success Transaction Retrieval Failure', function(done) {
      nock(process.env.TOKEN_CONNECT_URI as string).post("").reply(200, { 
        access_token: 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjE0NTk4OUIwNTdDOUMzMzg0MDc4MDBBOEJBNkNCOUZFQjMzRTk1MTAiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJGRm1Kc0ZmSnd6aEFlQUNvdW15NV9yTS1sUkEifQ.eyJuYmYiOjE1NDQxMjI5MzQsImV4cCI6MTU0NDEyNjUzNCwiaXNzIjoiaHR0cHM6Ly9hdXRoLnRydWVsYXllci5jb20iLCJhdWQiOlsiaHR0cHM6Ly9hdXRoLnRydWVsYXllci5jb20vcmVzb3VyY2VzIiwiaW5mb19hcGkiLCJhY2NvdW50c19hcGkiLCJ0cmFuc2FjdGlvbnNfYXBpIiwiYmFsYW5jZV9hcGkiLCJjYXJkc19hcGkiXSwiY2xpZW50X2lkIjoiZW1tYWRlbW8td2tjbyIsInN1YiI6InorM29GRC9ZNFdUeitoOFJzRE5BalZFTHhFS21JRWJaeExqWVVVOXF1VUk9IiwiYXV0aF90aW1lIjoxNTQ0MTIyOTMyLCJpZHAiOiJsb2NhbCIsImNvbm5lY3Rvcl9pZCI6Im1vY2siLCJjcmVkZW50aWFsc19rZXkiOiI0MzUxYmFmNTRjOTQzNGRlZTRmYWM3ZjY1NWFjMjAwNzgzNzk1NjAyMmQ2MzlhZWUwNTUxNzdlODNjMGI4YTQ0Iiwic2NvcGUiOlsiaW5mbyIsImFjY291bnRzIiwidHJhbnNhY3Rpb25zIiwiYmFsYW5jZSIsImNhcmRzIiwib2ZmbGluZV9hY2Nlc3MiXSwiYW1yIjpbInB3ZCJdfQ.mqjM9HbzJugpi3ZX7Pss28mHU09WchoiHOXwf0eQMKOKMHYfTjDYNfVlVB7fTMxp08aFgYMA9AVqijXf2p9gYf_uh1kuGwC6GWXSarpQHCBE-pmE7vSlHDcBxxvt3XiyZzHfIisPgIQ54XzL-ERuSd0SgjzOpasoGiruZlJqPQrMrrEfwvPmtkwFdmViHylKmcg8HdQKPtCZtnGK6FAEyskaTUDiO381QvGRNF_QCNjIL0ACU1nPYNWCL8AYKevdWHjUhXIRJvwYCyfifV21QlY4-36iDhv4QRdC3Gs3gNVPcH0D1YNiMrmbRHhMYzcPjqa01nxHHK8djqlQ3QGVNQ',
        expires_in: 3600,
        token_type: 'Bearer',
        refresh_token: 'ef700806eff99cc4e36dc9a52111d0f854dcd2ebbfe2f2140251386e3ef146aa'
      });

      const apiNock = nock(process.env.API_BASE_URI as string);

      apiNock.get("/info").reply(404, "No Information");
      apiNock.get("/accounts").reply(404, "No Accounts")

      request(app)
        .get(`/callback`)
        .query({
          code: "av",
          state: "cas",
          scope: "account targets"
           })
        .expect(400)
        .expect(response => {
          response.body === "Error Retrieving Customer Transactions";
        })
        .end(function(err, res) {
          if (err) return done(err);
          done();
        });
    });
  });
