import axios, { AxiosPromise, AxiosRequestConfig, AxiosResponse } from "axios";
import { LoginResponse, TokenRequest, TokenResponse } from "../models/models";
import Axios from "axios";
import { Customer, Account, Transaction, Provider } from "../entity/models";
import _, { Dictionary } from "lodash";
import { DatabaseService } from "./database";


export class ApiService {

    public tokens: TokenResponse;
    public tokenRequest: TokenRequest;
    private baseUrl: string;
    private databseservice: DatabaseService;

    constructor(baseUrl:string, databaseservice: DatabaseService) {
        this.baseUrl = baseUrl;
        this.tokens = new TokenResponse();
        this.databseservice = databaseservice;
    }
    public request_tokens(url: string, data: any, config: AxiosRequestConfig) {
        return axios.post<TokenResponse>(url, data, config);
    }

    public saveTokenRequest(tokenRequest: TokenRequest) {
        this.tokenRequest = tokenRequest;
    }

    public extract_login_response(responseDictionary: any): LoginResponse {
        const result = {
            code : responseDictionary.code,
            error : responseDictionary.error,
            scope : responseDictionary.scope,
            state: responseDictionary.state,
        };

        return result;
    }
    public updateTokens(tokenData: any) {
        this.tokens = {
            accessToken: tokenData.access_token,
            expiresIn: +tokenData.expires_in,
            refreshToken: tokenData.refresh_token,
            tokenType: tokenData.token_type,
        };
    }
    public getCustomerInfo() {
        const config: AxiosRequestConfig = this.getApiRequestConfiguration();
        return axios.get("/info", config);
    }

    public getCustomerAccounts() {
        const config: AxiosRequestConfig = this.getApiRequestConfiguration();
        return axios.get("/accounts", config);
    }

    public getAccountTransactions(account_id: string) {
        const config = this.getApiRequestConfiguration();
        return axios.get(`accounts/${account_id}/transactions`, config);
    }

    private getApiRequestConfiguration(): AxiosRequestConfig {
        return {
            baseURL: this.baseUrl,
            headers: {
                Authorization: `Bearer ${this.tokens.accessToken}`,
            },
        };
    }


    public getCustomerTransactions() {
           return Axios.all([this.getCustomerInfo(), this.getCustomerAccounts()])
            .then(Axios.spread((customerInfoAxiosResponse: AxiosResponse,
                                customerAccountAxiosReponse: AxiosResponse) => {
                
                let isSuccessful = customerInfoAxiosResponse.data.status as string === "Succeeded";
                
                const customerResults = (customerInfoAxiosResponse.data.results || [])
                                         .map((x:any) => Customer.mapCustomer(x));
                
                const latestCustomer = _.maxBy(customerResults, (x: Customer) => x.updateTimestamp) as Customer;

                isSuccessful = isSuccessful && latestCustomer && customerAccountAxiosReponse.data.status as string === "Succeeded";
                const customerAccounts: Account[] = (customerAccountAxiosReponse.data.results || [])
                                                        .map((x: any) => Account.mapAccount(x));
                
                latestCustomer.accounts = customerAccounts;
                
                return Promise.resolve(latestCustomer);

            }))
            .then(async (customer: Customer) => {
                const connection = await this.databseservice.getDatabaseConnection();
                let customerAccounts = customer.accounts as Account[];
                
                const accounts = await connection.getRepository(Account).createQueryBuilder("account")
                                                 .innerJoinAndSelect("account.customer", "customer")
                                                 .andWhereInIds(customerAccounts.map(x=>x.accountId)).getMany();

                if (accounts && accounts.length> 0) {
                    console.log("Accounts should have value", accounts);
                    customer.id = (accounts[0].customer as Customer).id;
                }

                await connection.getRepository(Customer).save(customer);
                console.log(customer);
                customerAccounts = customerAccounts.map(x => {
                    x.customer = customer;
                    return x;
                });

                const uniqueProviders = _.uniqBy(customerAccounts.map(x=>x.provider as Provider).filter(x => x !== undefined), x => x.providerId);

                await connection.getRepository(Provider).save(uniqueProviders);

                await connection.getRepository(Account).save(customerAccounts);

                const transactionRequests = customerAccounts.map(x => this.getAccountTransactions(x.accountId));
                const transactionResponses = await Axios.all(transactionRequests);

                const allTransactions: any[] = [];

                for(let i =0; i < transactionResponses.length; i++) {
                    const currentTransactionResponse = transactionResponses[i];
                    const currentAccount = customerAccounts[i];
                    const currentResponses = currentTransactionResponse.data.results
                        .map((x: any) => Transaction.mapTransaction(x))
                        .map((x: Transaction) => x.includeAccount(currentAccount));

                        allTransactions.push(...currentResponses);
                }

                return Promise.all([Promise.resolve(customer), connection.getRepository(Transaction).save(allTransactions)]);
            })
    }

    async getGroupedTransactions(customerId: any): Promise<Dictionary<Transaction[]>> {
        const connection = await this.databseservice.getDatabaseConnection();
        const transaction = await connection.getRepository(Transaction)
                                .createQueryBuilder("transaction")
                                .innerJoinAndSelect("transaction.account", "account")
                                .innerJoin("account.customer", "customer")
                                .having("customer.Id = :customerId")
                                .setParameter("customerId", customerId)
                                .addGroupBy("account.accountId")
                                .addGroupBy("transaction.transactionId")
                                .addGroupBy("customer.id")
                                .printSql()
                                .getMany();

        const groupedTransactions = _.groupBy(transaction, (x: Transaction) => (x.account as Account).accountId)
        return groupedTransactions
    }

}
