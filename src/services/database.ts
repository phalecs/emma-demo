import { Connection, ConnectionOptions, createConnection, getConnectionOptions } from "typeorm";

export class DatabaseService {
    private connection: Promise<Connection>;

    constructor(connectionOptions: Promise<ConnectionOptions>) {
        this.connection = connectionOptions.then(options => createConnection(options));
    }

    public getDatabaseConnection(): Promise<Connection> {
        return this.connection;
    }
}
