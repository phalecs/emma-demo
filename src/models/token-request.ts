import FormData from "form-data";

export class TokenRequest {
    public clientId: string;
    public clientSecret: string;
    public code: string;
    public grantType: string;
    public redirectUri: string;

    constructor(code: string, grantType: string = "authorization_code") {
        this.code = code;
        this.grantType = grantType;
        this.clientId = process.env.TRUE_LAYER_CLIENT_ID as string;
        this.clientSecret = process.env.TRUE_LAYER_CLIENT_SECRET as string;
        this.redirectUri = process.env.REDIRECT_URI as string;
    }

    public toFormData() {
        const result = new FormData();
        result.append("client_id",  this.clientId);
        result.append("client_secret", this.clientSecret);
        result.append("code", this.code);
        result.append("redirect_uri", this.redirectUri);
        result.append("grant_type", this.grantType);
        return result;
    }
}
