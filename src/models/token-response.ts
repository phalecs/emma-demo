export class TokenResponse {
    public accessToken: string;
    public expiresIn: number;
    public tokenType: string;
    public refreshToken: string;
    constructor() {
        this.accessToken = "";
        this.expiresIn = 0;
        this.tokenType = "";
        this.refreshToken = "";
    }
}
