export class LoginResponse {
    public code: string;
    public state: string;
    public scope: string;
    public error: string;

    constructor() {
        this.code = "";
        this.state = "";
        this.scope = "";
        this.error = "";
    }
}
