import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialDatabase1543968895408 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "customer" ("id" SERIAL NOT NULL, "fullName" character varying NOT NULL, "dateOfBirth" TIMESTAMP, "emails" text array, "phones" text array, "updateTimestamp" TIMESTAMP NOT NULL, CONSTRAINT "PK_a7a13f4cacb744524e44dfdad32" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "provider" ("displayName" character varying NOT NULL, "logoUri" character varying NOT NULL, "providerId" character varying NOT NULL, CONSTRAINT "PK_c0e2db5ac09483b145b092757b1" PRIMARY KEY ("providerId"))`);
        await queryRunner.query(`CREATE TABLE "transaction" ("transactionId" character varying NOT NULL, "timestamp" TIMESTAMP NOT NULL, "description" character varying NOT NULL, "transactionType" character varying NOT NULL, "transactionCategory" character varying NOT NULL, "transactionClassification" text array, "merchantName" character varying, "amount" numeric NOT NULL, "currency" character varying NOT NULL, "meta" text NOT NULL, "accountAccountId" character varying, CONSTRAINT "PK_bdcf2c929b61c0935576652d9b0" PRIMARY KEY ("transactionId"))`);
        await queryRunner.query(`CREATE TABLE "account" ("accountId" character varying NOT NULL, "accountType" character varying NOT NULL, "currency" character varying NOT NULL, "displayName" character varying NOT NULL, "iban" character varying, "accountNumber" character varying NOT NULL, "sortCode" character varying NOT NULL, "swiftBic" character varying NOT NULL, "updateTimestamp" TIMESTAMP NOT NULL, "customerId" integer, "providerProviderId" character varying, CONSTRAINT "PK_b1a9fdd281787a66a213f5b725b" PRIMARY KEY ("accountId"))`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD CONSTRAINT "FK_3c8747d9c9d4f264e2ca8653f71" FOREIGN KEY ("accountAccountId") REFERENCES "account"("accountId")`);
        await queryRunner.query(`ALTER TABLE "account" ADD CONSTRAINT "FK_295cfbf4cba51e0e67a6984ab8f" FOREIGN KEY ("customerId") REFERENCES "customer"("id")`);
        await queryRunner.query(`ALTER TABLE "account" ADD CONSTRAINT "FK_6544def892297b8e8d887549673" FOREIGN KEY ("providerProviderId") REFERENCES "provider"("providerId")`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "account" DROP CONSTRAINT "FK_6544def892297b8e8d887549673"`);
        await queryRunner.query(`ALTER TABLE "account" DROP CONSTRAINT "FK_295cfbf4cba51e0e67a6984ab8f"`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP CONSTRAINT "FK_3c8747d9c9d4f264e2ca8653f71"`);
        await queryRunner.query(`DROP TABLE "account"`);
        await queryRunner.query(`DROP TABLE "transaction"`);
        await queryRunner.query(`DROP TABLE "provider"`);
        await queryRunner.query(`DROP TABLE "customer"`);
    }

}
