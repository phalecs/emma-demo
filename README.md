# Awesome Project Build with TypeORM

```
cp sample.env .env
```

Edit .env file to contain details of your API and database respectively. Ensure database values in .env file match those in docker-compose for postgres database. While this violates the policy to exclude secrets. In a production infrastructure the database will be managed separately.

Build docker-compose image

docker-compose build

Tests are run during the dockerization process automatically. tests.env is used for running tests. This will be checked into the source code. The values should only reflect sample/mock values since no external dependencies should be used for the tests. Nock is introduced to avoid external HTTP calls.

Endpoints
/login => Authenticate.
/customer/{customer_id}/transactions
/customer/{customer_id}/transactions
